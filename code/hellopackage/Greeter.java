package hellopackage;
import java.util.Scanner;
import java.util.Random;
import secondpackage.Utilities;

public class Greeter {
    public static void main(String[] args) {
        Scanner reader = new Scanner(System.in);
        java.util.Random rand = new java.util.Random();
        System.out.println("Enter an integer value");
        int x = reader.nextInt();
        Utilities u = new Utilities();
        System.out.println(u.doubleMe(x));
    }
}